package com.trileuco.deathstarrecords.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

import com.trileuco.deathstarrecords.domain.cinema.Film;
import com.trileuco.deathstarrecords.domain.empire.ImperialPerson;
import com.trileuco.deathstarrecords.domain.empire.ImperialPersonPaginated;
import com.trileuco.deathstarrecords.domain.empire.ImperialPersonWrapper;
import com.trileuco.deathstarrecords.domain.galaxy.Planet;
import com.trileuco.deathstarrecords.domain.hangar.Vehicle;
import com.trileuco.deathstarrecords.service.empire.RecordsServiceAPI;
import com.trileuco.deathstarrecords.service.empire.RecordsServiceAPIGenerator;
import com.trileuco.deathstarrecords.util.empire.exception.LostInSpaceException;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import retrofit2.Call;

import retrofit2.Response;

@RestController
public class SwapiProxyController {

    private final static Logger LOG = Logger.getLogger(SwapiProxyController.class.getName());

    @GetMapping(value = "/person-info")
    @ExceptionHandler({ LostInSpaceException.class })
    public ImperialPerson findImperialPersonByName(String name) throws Exception {
        ImperialPerson imperialPerson = null;
        RecordsServiceAPI service = RecordsServiceAPIGenerator.createService(RecordsServiceAPI.class);
        try {
            ImperialPersonWrapper imperialPersonWrapper = getPersonByName(service, name);
            List<Film> filmList = getCharacterFilms(service, imperialPersonWrapper.getFilmsUrls());
            String maxSpeedVehicleName = getMaxSpeedVehicle(service, imperialPersonWrapper.getVehiclesUrls());
            String birthPlanetName = getBirthPlanetName(service, imperialPersonWrapper.getHomeWorldUrl());
            imperialPerson = ImperialPerson.build(imperialPersonWrapper, maxSpeedVehicleName, filmList,
                    birthPlanetName);
        } catch (LostInSpaceException e) {
            throw new LostInSpaceException(e.getMessage());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return imperialPerson;
    }

    private ImperialPersonWrapper getPersonByName(RecordsServiceAPI service, String name) throws Exception {
        ImperialPersonWrapper personLookinFor = null;
        try {
            Call<ImperialPersonPaginated> imperialPersonAsync = service.getPeople(name);
            Response<ImperialPersonPaginated> aux = imperialPersonAsync.execute();
            ImperialPersonPaginated imperialPersonPaginated = aux.body();
            if (imperialPersonPaginated.getCount() < 1) {
                LOG.info("Not found");
                throw new LostInSpaceException("Character not found");
            }
            personLookinFor = imperialPersonPaginated.getResults().get(0);
        } catch (IOException e) {
            throw new Exception("Error retriving server information");
        }
        return personLookinFor;
    }

    private String getBirthPlanetName(RecordsServiceAPI service, String planetUrl) {
        try {
            Call<Planet> syncPlanet = service.getPlanetFromUrl(planetUrl);
            Planet planet = syncPlanet.execute().body();
            return planet.getName();
        } catch (IOException e) {
            LOG.info(e.getMessage());
        }
        return "";
    }

    private String getMaxSpeedVehicle(RecordsServiceAPI service, List<String> vehicleUrls) {
        List<Vehicle> vehicleList = new ArrayList<Vehicle>();
        Vehicle maxSpeedVehicle = new Vehicle();
        try {
            for (String url : vehicleUrls) {
                Call<Vehicle> syncVehicle = service.getVehiclesFromUrl(url);
                Vehicle vehicle = syncVehicle.execute().body();
                vehicleList.add(vehicle);
            }
            maxSpeedVehicle = vehicleList.stream().max(Comparator.comparing(Vehicle::getAtmosperingSpeed))
                    .orElseThrow(NoSuchElementException::new);
            return maxSpeedVehicle.getName();
        } catch (IOException e) {
            LOG.info(e.getMessage());
        }
        return "";
    }

    private List<Film> getCharacterFilms(RecordsServiceAPI service, List<String> filmsUrls) {
        List<Film> filmList = new ArrayList<Film>();
        try {
            for (String url : filmsUrls) {
                Call<Film> syncFilm = service.getFilmsFromUrl(url);
                Film film = syncFilm.execute().body();
                filmList.add(film);
            }
        } catch (IOException e) {
            LOG.info(e.getMessage());
        }
        return filmList;
    }

    private ImperialPerson buildImerialPerson(ImperialPersonWrapper imperialPersonWrapper, String maxSpeedVehicleName,
            List<Film> filmList, String planetName) {
        ImperialPerson imperialPerson = new ImperialPerson();
        imperialPerson.setName(imperialPersonWrapper.getName());
        imperialPerson.setGender(imperialPersonWrapper.getGender());
        imperialPerson.setBirthPlanet(planetName);
        imperialPerson.setBirthYear(imperialPersonWrapper.getBirthYear());
        imperialPerson.setFastestVehicleDriven(maxSpeedVehicleName);
        imperialPerson.setFilms(filmList);
        return imperialPerson;
    }
}