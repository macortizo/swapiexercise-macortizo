package com.trileuco.deathstarrecords.util.empire.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Character not found")
public class LostInSpaceException extends Exception {

    private static final long serialVersionUID = -5021152281438788714L;

    public LostInSpaceException(String errorMsg) {
        super(errorMsg);
    }
}