package com.trileuco.deathstarrecords.util.empire;

import java.util.List;
import java.util.stream.Collectors;

import com.trileuco.deathstarrecords.domain.empire.ImperialPerson;

import org.springframework.stereotype.Component;

@Component
public class ImperialPersonUtil {

    public List<ImperialPerson> filterByName(List<ImperialPerson> list, String name) {
        return list.stream().filter(person -> person.getName().equals(name)).collect(Collectors.toList());
    }
}