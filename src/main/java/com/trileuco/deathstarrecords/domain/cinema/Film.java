package com.trileuco.deathstarrecords.domain.cinema;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import com.google.gson.annotations.SerializedName;

public class Film implements Serializable {

    private static final long serialVersionUID = 356271502142578627L;

    @SerializedName("title")
    private String name;

    @SerializedName("release_date")
    private String releaseDate;

    public Film() {
    }

    public Film(String name, String releaseDate) {
        this.name = name;
        this.releaseDate = releaseDate;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReleaseDate() {
        return this.releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Film name(String name) {
        this.name = name;
        return this;
    }

    public Film releaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Film)) {
            return false;
        }
        Film film = (Film) o;
        return Objects.equals(name, film.name) && Objects.equals(releaseDate, film.releaseDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, releaseDate);
    }

    @Override
    public String toString() {
        return "{" + " name='" + getName() + "'" + ", releaseDate='" + getReleaseDate() + "'" + "}";
    }
}