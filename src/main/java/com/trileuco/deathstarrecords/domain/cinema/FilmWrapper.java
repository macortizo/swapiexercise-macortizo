package com.trileuco.deathstarrecords.domain.cinema;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class FilmWrapper implements Serializable {

    private static final long serialVersionUID = 2870751142325613733L;

    @SerializedName("title")
    private String name;

    @SerializedName("release_date")
    private String releaseDate;

}