package com.trileuco.deathstarrecords.domain.galaxy;

import java.io.Serializable;
import java.util.Objects;

public class Planet implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1316796158711800738L;
    private String name;

    public Planet() {
    }

    public Planet(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Planet name(String name) {
        this.name = name;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Planet)) {
            return false;
        }
        Planet planet = (Planet) o;
        return Objects.equals(name, planet.name);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name);
    }

    @Override
    public String toString() {
        return "{" + " name='" + getName() + "'" + "}";
    }
}
