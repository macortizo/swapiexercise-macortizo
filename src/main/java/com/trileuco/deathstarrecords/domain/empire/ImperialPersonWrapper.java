package com.trileuco.deathstarrecords.domain.empire;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.google.gson.annotations.SerializedName;

public class ImperialPersonWrapper implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    @SerializedName("birth_year")
    private String birthYear;
    @SerializedName("gender")
    private String gender;
    @SerializedName("films")
    private List<String> filmsUrls;
    @SerializedName("vehicles")
    private List<String> vehiclesUrls;
    @SerializedName("homeworld")
    private String homeWorldUrl;

    public ImperialPersonWrapper() {
    }

    public ImperialPersonWrapper(String name, String birthYear, String gender, List<String> filmsUrls,
            List<String> vehiclesUrls, String homeWorldUrl) {
        this.name = name;
        this.birthYear = birthYear;
        this.gender = gender;
        this.filmsUrls = filmsUrls;
        this.vehiclesUrls = vehiclesUrls;
        this.homeWorldUrl = homeWorldUrl;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthYear() {
        return this.birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<String> getFilmsUrls() {
        return this.filmsUrls;
    }

    public void setFilmsUrls(List<String> filmsUrls) {
        this.filmsUrls = filmsUrls;
    }

    public List<String> getVehiclesUrls() {
        return this.vehiclesUrls;
    }

    public void setVehiclesUrls(List<String> vehiclesUrls) {
        this.vehiclesUrls = vehiclesUrls;
    }

    public String getHomeWorldUrl() {
        return this.homeWorldUrl;
    }

    public void setHomeWorldUrl(String homeWorldUrl) {
        this.homeWorldUrl = homeWorldUrl;
    }

    public ImperialPersonWrapper name(String name) {
        this.name = name;
        return this;
    }

    public ImperialPersonWrapper birthYear(String birthYear) {
        this.birthYear = birthYear;
        return this;
    }

    public ImperialPersonWrapper gender(String gender) {
        this.gender = gender;
        return this;
    }

    public ImperialPersonWrapper filmsUrls(List<String> filmsUrls) {
        this.filmsUrls = filmsUrls;
        return this;
    }

    public ImperialPersonWrapper vehiclesUrls(List<String> vehiclesUrls) {
        this.vehiclesUrls = vehiclesUrls;
        return this;
    }

    public ImperialPersonWrapper homeWorldUrl(String homeWorldUrl) {
        this.homeWorldUrl = homeWorldUrl;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof ImperialPersonWrapper)) {
            return false;
        }
        ImperialPersonWrapper imperialPersonWrapper = (ImperialPersonWrapper) o;
        return Objects.equals(name, imperialPersonWrapper.name)
                && Objects.equals(birthYear, imperialPersonWrapper.birthYear)
                && Objects.equals(gender, imperialPersonWrapper.gender)
                && Objects.equals(filmsUrls, imperialPersonWrapper.filmsUrls)
                && Objects.equals(vehiclesUrls, imperialPersonWrapper.vehiclesUrls)
                && Objects.equals(homeWorldUrl, imperialPersonWrapper.homeWorldUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, birthYear, gender, filmsUrls, vehiclesUrls, homeWorldUrl);
    }

    @Override
    public String toString() {
        return "{" + " name='" + getName() + "'" + ", birthYear='" + getBirthYear() + "'" + ", gender='" + getGender()
                + "'" + ", filmsUrls='" + getFilmsUrls() + "'" + ", vehiclesUrls='" + getVehiclesUrls() + "'"
                + ", homeWorldUrl='" + getHomeWorldUrl() + "'" + "}";
    }

}