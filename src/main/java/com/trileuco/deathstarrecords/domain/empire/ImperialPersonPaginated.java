package com.trileuco.deathstarrecords.domain.empire;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ImperialPersonPaginated implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer count;
    private String next;
    private String previous;
    List<ImperialPersonWrapper> results;

    public ImperialPersonPaginated() {
    }

    public ImperialPersonPaginated(Integer count, String next, String previous, List<ImperialPersonWrapper> results) {
        this.count = count;
        this.next = next;
        this.previous = previous;
        this.results = results;
    }

    public Integer getCount() {
        return this.count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getNext() {
        return this.next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return this.previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public List<ImperialPersonWrapper> getResults() {
        return this.results;
    }

    public void setResults(List<ImperialPersonWrapper> results) {
        this.results = results;
    }

    public ImperialPersonPaginated count(Integer count) {
        this.count = count;
        return this;
    }

    public ImperialPersonPaginated next(String next) {
        this.next = next;
        return this;
    }

    public ImperialPersonPaginated previous(String previous) {
        this.previous = previous;
        return this;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof ImperialPersonPaginated)) {
            return false;
        }
        ImperialPersonPaginated imperialPersonPaginated = (ImperialPersonPaginated) o;
        return Objects.equals(count, imperialPersonPaginated.count)
                && Objects.equals(next, imperialPersonPaginated.next)
                && Objects.equals(previous, imperialPersonPaginated.previous)
                && Objects.equals(results, imperialPersonPaginated.results);
    }

    @Override
    public int hashCode() {
        return Objects.hash(count, next, previous, results);
    }

    @Override
    public String toString() {
        return "{" + " count='" + getCount() + "'" + ", next='" + getNext() + "'" + ", previous='" + getPrevious() + "'"
                + ", results='" + getResults() + "'" + "}";
    }

}