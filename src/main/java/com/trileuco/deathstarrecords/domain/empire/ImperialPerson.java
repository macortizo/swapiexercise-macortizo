package com.trileuco.deathstarrecords.domain.empire;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;
import com.trileuco.deathstarrecords.domain.cinema.Film;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ImperialPerson implements Serializable {

    private static final long serialVersionUID = -8237780261689936692L;
    private String name;
    @SerializedName("birth_year")
    private String birthYear;
    private String gender;
    @SerializedName("planet_name")
    private String birthPlanet;
    @SerializedName("fastest_vehicle_driven")
    private String fastestVehicleDriven;
    private List<Film> films;

    public ImperialPerson() {
    }

    public ImperialPerson(String name, String birthYear, String gender, String birthPlanet, String fastestVehicleDriven,
            List<Film> films) {
        this.name = name;
        this.birthYear = birthYear;
        this.gender = gender;
        this.birthPlanet = birthPlanet;
        this.fastestVehicleDriven = fastestVehicleDriven;
        this.films = films;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthYear() {
        return this.birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthPlanet() {
        return this.birthPlanet;
    }

    public void setBirthPlanet(String birthPlanet) {
        this.birthPlanet = birthPlanet;
    }

    public String getFastestVehicleDriven() {
        return this.fastestVehicleDriven;
    }

    public void setFastestVehicleDriven(String fastestVehicleDriven) {
        this.fastestVehicleDriven = fastestVehicleDriven;
    }

    public List<Film> getFilms() {
        return this.films;
    }

    public void setFilms(List<Film> films) {
        this.films = films;
    }

    public ImperialPerson name(String name) {
        this.name = name;
        return this;
    }

    public ImperialPerson birthYear(String birthYear) {
        this.birthYear = birthYear;
        return this;
    }

    public ImperialPerson gender(String gender) {
        this.gender = gender;
        return this;
    }

    public ImperialPerson birthPlanet(String birthPlanet) {
        this.birthPlanet = birthPlanet;
        return this;
    }

    public ImperialPerson fastestVehicleDriven(String fastestVehicleDriven) {
        this.fastestVehicleDriven = fastestVehicleDriven;
        return this;
    }

    public ImperialPerson films(List<Film> films) {
        this.films = films;
        return this;
    }

    public static ImperialPerson build(ImperialPersonWrapper imperialPersonWrapper, String maxSpeedVehicleName,
            List<Film> filmList, String planetName) {
        ImperialPerson imperialPerson = new ImperialPerson();
        imperialPerson.setName(imperialPersonWrapper.getName());
        imperialPerson.setGender(imperialPersonWrapper.getGender());
        imperialPerson.setBirthPlanet(planetName);
        imperialPerson.setBirthYear(imperialPersonWrapper.getBirthYear());
        imperialPerson.setFastestVehicleDriven(maxSpeedVehicleName);
        imperialPerson.setFilms(filmList);
        return imperialPerson;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof ImperialPerson)) {
            return false;
        }
        ImperialPerson imperialPerson = (ImperialPerson) o;
        return Objects.equals(name, imperialPerson.name) && Objects.equals(birthYear, imperialPerson.birthYear)
                && Objects.equals(gender, imperialPerson.gender)
                && Objects.equals(birthPlanet, imperialPerson.birthPlanet)
                && Objects.equals(fastestVehicleDriven, imperialPerson.fastestVehicleDriven)
                && Objects.equals(films, imperialPerson.films);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, birthYear, gender, birthPlanet, fastestVehicleDriven, films);
    }

    @Override
    public String toString() {
        return "{" + " name='" + getName() + "'" + ", birthYear='" + getBirthYear() + "'" + ", gender='" + getGender()
                + "'" + ", birthPlanet='" + getBirthPlanet() + "'" + ", fastestVehicleDriven='"
                + getFastestVehicleDriven() + "'" + ", films='" + getFilms() + "'" + "}";
    }

}