package com.trileuco.deathstarrecords.domain.hangar;

import com.trileuco.deathstarrecords.domain.empire.ImperialPerson;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.google.gson.annotations.SerializedName;

public class Vehicle implements Serializable {

    private static final long serialVersionUID = 2000316556609217998L;
    private String name;
    private String model;
    private String manufacturer;
    private String cost;
    private Double length;
    @SerializedName("max_atmosphering_speed")
    private Double atmosperingSpeed;
    private Integer crew;
    private Integer passengers;
    private Integer cargoCapacity;
    private String consumables;
    private String vehicleClass;
    private Date created;
    private Date edited;

    public Vehicle() {
    }

    public Vehicle(String name, String model, String manufacturer, String cost, Double length, Double atmosperingSpeed,
            Integer crew, Integer passengers, Integer cargoCapacity, String consumables, String vehicleClass,
            Date created, Date edited) {
        this.name = name;
        this.model = model;
        this.manufacturer = manufacturer;
        this.cost = cost;
        this.length = length;
        this.atmosperingSpeed = atmosperingSpeed;
        this.crew = crew;
        this.passengers = passengers;
        this.cargoCapacity = cargoCapacity;
        this.consumables = consumables;
        this.vehicleClass = vehicleClass;
        this.created = created;
        this.edited = edited;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getCost() {
        return this.cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public Double getLength() {
        return this.length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getAtmosperingSpeed() {
        return this.atmosperingSpeed;
    }

    public void setAtmosperingSpeed(Double atmosperingSpeed) {
        this.atmosperingSpeed = atmosperingSpeed;
    }

    public Integer getCrew() {
        return this.crew;
    }

    public void setCrew(Integer crew) {
        this.crew = crew;
    }

    public Integer getPassengers() {
        return this.passengers;
    }

    public void setPassengers(Integer passengers) {
        this.passengers = passengers;
    }

    public Integer getCargoCapacity() {
        return this.cargoCapacity;
    }

    public void setCargoCapacity(Integer cargoCapacity) {
        this.cargoCapacity = cargoCapacity;
    }

    public String getConsumables() {
        return this.consumables;
    }

    public void setConsumables(String consumables) {
        this.consumables = consumables;
    }

    public String getVehicleClass() {
        return this.vehicleClass;
    }

    public void setVehicleClass(String vehicleClass) {
        this.vehicleClass = vehicleClass;
    }

    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getEdited() {
        return this.edited;
    }

    public void setEdited(Date edited) {
        this.edited = edited;
    }

    public Vehicle name(String name) {
        this.name = name;
        return this;
    }

    public Vehicle model(String model) {
        this.model = model;
        return this;
    }

    public Vehicle manufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
        return this;
    }

    public Vehicle cost(String cost) {
        this.cost = cost;
        return this;
    }

    public Vehicle length(Double length) {
        this.length = length;
        return this;
    }

    public Vehicle atmosperingSpeed(Double atmosperingSpeed) {
        this.atmosperingSpeed = atmosperingSpeed;
        return this;
    }

    public Vehicle crew(Integer crew) {
        this.crew = crew;
        return this;
    }

    public Vehicle passengers(Integer passengers) {
        this.passengers = passengers;
        return this;
    }

    public Vehicle cargoCapacity(Integer cargoCapacity) {
        this.cargoCapacity = cargoCapacity;
        return this;
    }

    public Vehicle consumables(String consumables) {
        this.consumables = consumables;
        return this;
    }

    public Vehicle vehicleClass(String vehicleClass) {
        this.vehicleClass = vehicleClass;
        return this;
    }

    public Vehicle created(Date created) {
        this.created = created;
        return this;
    }

    public Vehicle edited(Date edited) {
        this.edited = edited;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Vehicle)) {
            return false;
        }
        Vehicle vehicle = (Vehicle) o;
        return Objects.equals(name, vehicle.name) && Objects.equals(model, vehicle.model)
                && Objects.equals(manufacturer, vehicle.manufacturer) && Objects.equals(cost, vehicle.cost)
                && Objects.equals(length, vehicle.length) && Objects.equals(atmosperingSpeed, vehicle.atmosperingSpeed)
                && Objects.equals(crew, vehicle.crew) && Objects.equals(passengers, vehicle.passengers)
                && Objects.equals(cargoCapacity, vehicle.cargoCapacity)
                && Objects.equals(consumables, vehicle.consumables)
                && Objects.equals(vehicleClass, vehicle.vehicleClass) && Objects.equals(created, vehicle.created)
                && Objects.equals(edited, vehicle.edited);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, model, manufacturer, cost, length, atmosperingSpeed, crew, passengers, cargoCapacity,
                consumables, vehicleClass, created, edited);
    }

    @Override
    public String toString() {
        return "{" + " name='" + getName() + "'" + ", model='" + getModel() + "'" + ", manufacturer='"
                + getManufacturer() + "'" + ", cost='" + getCost() + "'" + ", length='" + getLength() + "'"
                + ", atmosperingSpeed='" + getAtmosperingSpeed() + "'" + ", crew='" + getCrew() + "'" + ", passengers='"
                + getPassengers() + "'" + ", cargoCapacity='" + getCargoCapacity() + "'" + ", consumables='"
                + getConsumables() + "'" + ", vehicleClass='" + getVehicleClass() + "'" + ", created='" + getCreated()
                + "'" + ", edited='" + getEdited() + "'" + "}";
    }
}