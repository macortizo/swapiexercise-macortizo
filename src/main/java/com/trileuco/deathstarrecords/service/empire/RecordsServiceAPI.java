package com.trileuco.deathstarrecords.service.empire;

import com.trileuco.deathstarrecords.domain.cinema.Film;
import com.trileuco.deathstarrecords.domain.empire.ImperialPersonPaginated;
import com.trileuco.deathstarrecords.domain.galaxy.Planet;
import com.trileuco.deathstarrecords.domain.hangar.Vehicle;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface RecordsServiceAPI {

    @GET("/api/people")
    public Call<ImperialPersonPaginated> getPeople(@Query("search") String name);

    @GET("/api/films")
    public Call<Film> listFilms();

    @GET("/api/films/{id}")
    public Call<Film> getFilm(@Path("id") int id);

    @GET
    public Call<Film> getFilmsFromUrl(@Url String url);

    @GET
    public Call<Vehicle> getVehiclesFromUrl(@Url String url);

    @GET
    public Call<Planet> getPlanetFromUrl(@Url String url);
}