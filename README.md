-- Dependencias:

Java: 1.8
Gradle: 3.5
Spring-Boot: 1.5
Retrofit: 2.0

-- Ejecución:

1. gradle build
2. ./gradlew bootRun

-- Deploy
